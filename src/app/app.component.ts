import { Component, OnInit } from '@angular/core';

declare var $:any;
declare var jQuery:any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  lat: number = 60.938713;
  lng: number = 76.534020;
  zoom: number = 16;

  lat1: number = 59.937048;
  lng1: number = 30.312229;
  zoom1: number = 16;


  ngOnInit(){

  //Sidenav

    $( document ).ready(function(){
      $(".button-collapse").sideNav();
    });
   
  //Slider

    $(document).ready(function(){
      $('.slider').slider({full_width: true, indicators: false});
    });

$("#myNextNavButton").click(function(){
 $('.slider').slider('next');
});

$("#myPrevNavButton").click(function(){
 $('.slider').slider('prev');
});

$("#myPauseNavButton").click(function(){
  $('.slider').slider('pause');
});

$("#myStartNavButton").click(function(){
  $('.slider').slider('start');
});

  // Start Carrousel

    $('.carousel.carousel-slider').carousel({
      fullWidth: true,
      indicators: false
    });

 // Move Next Carousel

    $('.moveNextCarousel').click(function(e){
      e.preventDefault();
      e.stopPropagation();
      $('.carousel').carousel('next');
    });

 // Move Prev Carousel

    $('.movePrevCarousel').click(function(e){
      e.preventDefault();
      e.stopPropagation();
      $('.carousel').carousel('prev');
    });

    $(document).ready(function(){
      $('.scrollspy').scrollSpy();
    });
          
  }
  
}

